import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { useAuth } from '../auth/authUser';

const NavbarComponent = () => {
    const authUser = useAuth();
    const handleLogout = () => {
        authUser?.setUserToken(null)
    }
    return (
        <Navbar bg="light" expand="lg" className='mb-2 h3'>
            <Container>
                <Navbar.Brand href=" "> Todos</Navbar.Brand>
                <Nav.Item>
                    {authUser?.userToken && <button className='btn btn-secondary' onClick={handleLogout}>Logout </button>}
                </Nav.Item>
            </Container>
        </Navbar>
    )
}

export default NavbarComponent